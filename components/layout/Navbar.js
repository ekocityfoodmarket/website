import {useState, useEffect} from 'react';
import Link from '../utility/ActiveLink';
// import Link from 'next/link';
import Head from 'next/head'


const Navbar = (props)=>{
    const [toggleNav, setToggleNav] = useState({
        condition: false
      });
// console.log(props)
  const burgerToggle =() =>{
    setToggleNav({
        condition: !toggleNav.condition
      })
    }
    return(
        <div className="device">
            <Head>
                {/* <!-- Hotjar Tracking Code for https://ekocityfoodmart-5wvbtthup.now.sh/ --> */}
                <script
                    dangerouslySetInnerHTML={{
                    __html: `
                        (function(h,o,t,j,a,r){
                            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                            h._hjSettings={hjid:1846018,hjsv:6};
                            a=o.getElementsByTagName('head')[0];
                            r=o.createElement('script');r.async=1;
                            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                            a.appendChild(r);
                        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
                        `,
                    }}
                    />
                    {/*Google analytics */}
                    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-168714587-1"></script>
                    <script
                        dangerouslySetInnerHTML={{
                        __html: `
                        window.dataLayer = window.dataLayer || [];
                        function gtag(){dataLayer.push(arguments);}
                        gtag('js', new Date());
                    
                        gtag('config', 'UA-168714587-1');
                            `,
                        }}
                    />
            </Head>
  
            <div>
                <div className="container_nav">
                    <div className="flex_">
                        <div className="logo_container">
                            <Link  href="/">
                                <img src="/logo.svg" className="App-logo" alt="logo" />
                            </Link>
                        </div>
                    
                        <nav className={toggleNav.condition ? 'main-nav is-activated' : 'main-nav '  } id="main-nav">
                            <ul>
                                <li>
                                    <Link activeClassName="active" href="/"><a>Home</a></Link>
                                </li>
                                <li>
                                    <Link activeClassName="active" href="/About"><a>About Farmcrowdy Foods</a></Link>
                                </li>
                                
                            </ul> 
                            <div className="mobile_app_"> 
                                <span>
                                Call to Order:
                                    <Link  prefetch={false} href="tel:+2348123570321"><a> +234 812 3570 321</a></Link>
                                </span>
                                
                                <a href="https://play.google.com/store/apps/details?id=com.farmcrowdyfoods" target="_blank" rel="noopener noreferrer"> 
                                    <img src="/Playstore.svg" alt="Playstore logo"/>
                                </a>
                                <a href="https://apps.apple.com/us/app/farmcrowdy-foods/id1512137851" target="_blank" rel="noopener noreferrer">
                                    <img src="/Appstore.svg" alt="Appstore logo"/>
                                </a>
                            </div>
                        </nav>
                    </div>
                    <ul className="app_ul" >
                        <li>
                            Call to Order:
                            <Link prefetch={false} href="tel:+2348123570321"><a> +234 812 3570 321</a></Link>
                        </li>
                        <li>
                            <a href="https://play.google.com/store/apps/details?id=com.farmcrowdyfoods" target="_blank" rel="noopener noreferrer"> 
                                <img src="/Playstore.svg" alt="Playstore logo"/>
                            </a>
                        </li>
                        <li>
                            <a href="https://apps.apple.com/us/app/farmcrowdy-foods/id1512137851" target="_blank" rel="noopener noreferrer">
                                <img src="/Appstore.svg" alt="Playstore logo"/>
                            </a>
                        </li>
                        
                    </ul> 
                    <button id="burger"  onClick={ burgerToggle } className={toggleNav.condition ? 'open-main-nav is-open' : 'open-main-nav '  }>
                        <span className="burger"></span>
                        <span className="burger-text">Menu</span>
                    </button>
                </div>
            </div>
        </div>
    )
}
export default Navbar