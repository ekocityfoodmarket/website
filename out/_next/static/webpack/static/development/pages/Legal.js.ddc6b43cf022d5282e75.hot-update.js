webpackHotUpdate("static/development/pages/Legal.js",{

/***/ "./pages/ScrollSpy.js":
/*!****************************!*\
  !*** ./pages/ScrollSpy.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_scrollspy__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-scrollspy */ "./node_modules/react-scrollspy/lib/scrollspy.js");
/* harmony import */ var react_scrollspy__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_scrollspy__WEBPACK_IMPORTED_MODULE_1__);
var _this = undefined,
    _jsxFileName = "/Users/farmcrowdyfront-end/Desktop/Projects/eko-foodmart/pages/ScrollSpy.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


var ScrollSpy = function ScrollSpy(props) {
  return __jsx("div", {
    className: "spy_grid",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4,
      columnNumber: 5
    }
  }, __jsx("div", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5,
      columnNumber: 8
    }
  }, __jsx("div", {
    className: "scroll_links",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 13
    }
  }, __jsx("h4", {
    className: "title",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 17
    }
  }, "Legal"), __jsx(react_scrollspy__WEBPACK_IMPORTED_MODULE_1___default.a, {
    items: ['privacy_policy', 'disclaimer', 'terms_of_use'],
    currentClassName: "is-current",
    offset: -150,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 17
    }
  }, __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 21
    }
  }, __jsx("a", {
    href: "#privacy_policy",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 25
    }
  }, "Privacy policy")), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 21
    }
  }, __jsx("a", {
    href: "#disclaimer",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 25
    }
  }, "Disclaimer")), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 21
    }
  }, __jsx("a", {
    href: "#terms_of_use",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 25
    }
  }, "Terms of Use"))))), __jsx("div", {
    className: "scroll_containers",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 9
    }
  }, __jsx("section", {
    id: "privacy_policy",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 13
    }
  }, __jsx("h4", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 17
    }
  }, "Privacy policy"), __jsx("span", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 17
    }
  }, "Last updated 4/04/2020"), __jsx("div", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 17
    }
  }, __jsx("h6", {
    className: "h6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 21
    }
  }, "Introduction"), __jsx("p", {
    className: "first_p",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 21
    }
  }, "The Eko Foodmart (", __jsx("span", {
    className: "span",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 62
    }
  }, "\u201CEko Foodmart\u201D, \u201Cwe\u201D, \u201Cus\u201D, \u201Cour\u201D"), "), recognizes your privacy rights as guaranteed under the 1999 Constitution of the Federal Republic of Nigeria; The Nigerian Data Protection Regulation, and other applicable privacy laws in Nigeria. Thus, it is important to us as a law-abiding organisation that your Personal Data is managed, processed and protected in accordance with the provisions of the applicable laws. In the course of our business and/or your engagement with us and third parties through our Platforms (this includes but are not limited to our websites, digital platforms, mobile applications, physical operations/offices, amongst others), we may process your Personal Data, subject however to the terms of this Policy. This Privacy Policy (\u201CPolicy\u201D) therefore explains our privacy practices with respect to how we collect, process your Personal Data and describes your rights as a user of any of our services and Platforms. "), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 21
    }
  }, "This Policy applies to all our Platforms, and all related sites, applications, services and tools regardless of how they are accessed or used. In this Privacy Policy, \u201Cwe\u201D, \u201Cus\u201D or \u201Cour\u201D refers to the Eko Foodmart its successors, agents and assigns, while \u201Cyou\u201D, \u201Cyour\u201D refers to you and/or any person who subscribes for, uses or authorizes the use of the Service."), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 21
    }
  }, "As a user of any of our Platforms, you accept this Privacy Policy when you sign up for, or use our products, services, content, technologies or functions offered on our Platforms and all related sites and services (", __jsx("span", {
    className: "span",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 239
    }
  }, "\u201CServices\u201D"), ")."), __jsx("p", {
    className: "sub_h",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 21
    }
  }, "Our Privacy Policy explains our user\u2019s privacy rights regarding:"), __jsx("ol", {
    type: "a",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 21
    }
  }, __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 25
    }
  }, "What information we collect"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 25
    }
  }, "Why we collect such information"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 25
    }
  }, "The use of such information"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 25
    }
  }, "The storage, sharing and protection of such information")), __jsx("h6", {
    className: "h6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 21
    }
  }, "Definitions"), __jsx("p", {
    className: "first_p",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 21
    }
  }, "For the purpose of this Policy:"), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 21
    }
  }, __jsx("span", {
    className: "h6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 24
    }
  }, "Personal Data/Information"), " means information relating to you, including your name, location data, online identifier address, email address, pins, passwords, bank details, and other unique identifiers such as but not limited to IP address, IMEI number, Bank Verification Number, IMSI Number, SIM and others. "), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 21
    }
  }, __jsx("span", {
    className: "h6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 24
    }
  }, "Process/Processing"), " means any operation or set of operations which is performed on your Personal Data or sets of Personal Data, whether or not by automated means, such as collection, recording, organisation, structuring, storage, adaptation or alteration, retrieval, consultation, use, disclosure by transmission, dissemination or otherwise making available, alignment or combination, restriction, erasure or destruction."), __jsx("h6", {
    className: "h6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 21
    }
  }, "Consent"), __jsx("p", {
    className: "first_p",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 21
    }
  }, "We kindly request that you carefully read through this Policy and click on the ", __jsx("span", {
    className: "h6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 123
    }
  }, "\u2018Agree\u2019"), " icon below, if you consent to the terms of this Policy, particularly with respect to the processing of your Personal Data. That notwithstanding, if you proceed to use any of our Platforms and services, it will deemed that you have provided your express consent to the terms of this Policy. "), __jsx("h6", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 21
    }
  }, "Age"), __jsx("p", {
    className: "first_p",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 21
    }
  }, "You must be at least 18 years old to use our services or any of our Platforms. Individuals under the age of 18, or applicable age of maturity, may utilize our Platforms services only with involvement of a parent or legal guardian, under such person\u2019s account. Regardless, all Personal Data which we may process shall be in accordance with this Policy and other applicable laws. "), __jsx("h6", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 21
    }
  }, "Collection of Personal Data"), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 21
    }
  }, "In the course of your engagement with us or with third parties through our Platforms, we may collect your Personal Data in the following ways: "), __jsx("ul", {
    className: "li_space",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 21
    }
  }, __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 25
    }
  }, __jsx("span", {
    className: "h6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 29
    }
  }, "Automatic Information collection:"), " We may automatically collect Information upon your engagement with us or our Platforms through your computer, mobile phones and other access devices. The information sent includes, but is not limited to, data about our pages you access, geo-location information; statistics on page views; traffic to and from our Platforms. "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 25
    }
  }, __jsx("span", {
    className: "h6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 29
    }
  }, "Information from downloads:"), " When you download or use our digital Platforms or access our site, we may receive non personal information from your mobile device about your location, number of downloads, geo location of downloads. You may disable your location in the settings menu of the device. "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 25
    }
  }, __jsx("span", {
    className: "h6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 29
    }
  }, "Physically or Digitally Provided Information:"), " We may collect and process your information when you create and/or update your account on our Platform; complete forms, questionnaires, surveys etc. When you use our Services, we may also collect information about your transactions and your activities. In addition, when you open an account on our Platform, we may collect information such as your contact information, which includes your name, address, phone number, email or other similar information; financial information such as your full bank account numbers and/or credit card or debit card numbers. "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 25
    }
  }, __jsx("span", {
    className: "h6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 29
    }
  }, "Third Parties:"), " We may also receive your information from third parties such as, guardians, financial institutions, identity verification services, vendors, and service providers etc. "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 25
    }
  }, __jsx("span", {
    className: "h6",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 29
    }
  }, "Social Media:"), "  We also receive Information through your engagements with us on social media sites (e.g., Facebook, Instagram, LinkedIn, Twitter, etc). This includes but are not limited to your replies to our posts, your comments, enquiries, messages to us, etc. We may also collect information from your public profile and updates on social media.  ")), __jsx("h6", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 21
    }
  }, "Accessing your Personal Information"), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 21
    }
  }, "When accessing our Platforms, we may collect information about you and your interactions with the Services to which we undertake to keep secure and confidential."), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 21
    }
  }, "We may offer you the ability to connect with our Platforms using a mobile device, either through a mobile application (App), computer, mobile optimized website, or by any other means. The provisions of this Privacy Policy apply to all such mobile access and use of mobile devices."), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51,
      columnNumber: 21
    }
  }, "When you download or use our mobile applications, or access one of our mobile optimized sites, we may receive information about your location and your mobile device, including a unique identifier for your device. We may use this information to provide you with location-based services, such as advertising, search results, and other personalized content. Most mobile devices allow you to control or disable location services in the device's setting's menu. If you have questions about how to disable your device\u2019s location services, we recommend you contact your mobile service carrier or the manufacture of your particular device."), __jsx("h6", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 21
    }
  }, "Using Your Personal Information"), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 21
    }
  }, "In the course of your engagements with us or through our Platforms, we collect personal information for various legal reasons, largely to enable us to personalize your experience and to provide a more efficient service to you. Some of the reasons we collect Information are to:"), __jsx("ul", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 21
    }
  }, __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 25
    }
  }, "provide services and customer support;"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 25
    }
  }, "process transactions, payment requests, and send notices about transactions; "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 25
    }
  }, "create an account with us for the provision or use of our services;"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 25
    }
  }, "communicate with you about your account or transactions with us and send you information or request feedback about features on our website and applications or changes to our policies;"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 25
    }
  }, "send you periodic emails and updates pertaining to our products and services;"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 25
    }
  }, "verify customers\u2019 identity, including during account creation and password reset processes; "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61,
      columnNumber: 25
    }
  }, "manage your account and provide you with efficient customer service,"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 25
    }
  }, "send you offers and promotions for our services and investment opportunities"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 25
    }
  }, "resolve disputes, process payments and troubleshoot problems; "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 25
    }
  }, "detect, investigate and prevent activities that may violate our policies or be illegal;"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 25
    }
  }, "manage risks, or to detect, prevent, and/or remediate fraud, violation of policies and applicable user agreements or other potentially prohibited or illegal activities;"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 25
    }
  }, "execute our contractual obligations to you; "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 25
    }
  }, "improve our services and functionality by customizing user experience; "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 25
    }
  }, "measure the performance of our services and improve their content and layout; "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69,
      columnNumber: 25
    }
  }, "manage and protect our information technology infrastructure; "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70,
      columnNumber: 25
    }
  }, "provide targeted marketing and advertising, provide service or transaction update notices, and deliver promotional offers based on communication preferences; "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 25
    }
  }, "obtain a means by which we may contact you; either by telephone, text (SMS), email messaging, social media, etc; "), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 25
    }
  }, "conduct background checks, compare information for accuracy and verify same with third parties;"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73,
      columnNumber: 25
    }
  }, "identify or address a violation and administer our policies and terms of use;"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 25
    }
  }, "comply with legal, contractual and regulatory obligations;"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 25
    }
  }, "execute your specific requests or use same for a specific purpose as you may instruct;"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 25
    }
  }, "investigate and respond to your complaints or enquiries;"), __jsx("li", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77,
      columnNumber: 25
    }
  }, "process your access to our services, Platforms, or functions from time to time;  ")), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79,
      columnNumber: 21
    }
  }, "If we intend to use any Personal Information in any manner that is not consistent with this Privacy Policy, you will be informed of such anticipated use prior to or at the time at which the Personal information is required and obtain your consent."))), __jsx("section", {
    id: "disclaimer",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 13
    }
  }, __jsx("h4", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 17
    }
  }, "Disclaimer"), __jsx("span", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 17
    }
  }, "Last updated 4/04/2020"), __jsx("div", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 17
    }
  }, __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 21
    }
  }, "Eko Foodmart is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes. We ensure that consumers obtain their products from hygienic environments and are processed in a condition fir for human consumption."), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 21
    }
  }, "Eko Foodmart is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes. We ensure that consumers obtain their products from hygienic environments and are processed in a condition fir for human consumption."), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 21
    }
  }, "Eko Foodmart is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes. We ensure that consumers obtain their products from hygienic environments and are processed in a condition fir for human consumption."))), __jsx("section", {
    id: "terms_of_use",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91,
      columnNumber: 13
    }
  }, __jsx("h4", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 92,
      columnNumber: 17
    }
  }, "Terms of Use"), __jsx("span", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 93,
      columnNumber: 17
    }
  }, "Last updated 4/04/2020"), __jsx("div", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 94,
      columnNumber: 17
    }
  }, __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95,
      columnNumber: 21
    }
  }, "Eko Foodmart is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes. We ensure that consumers obtain their products from hygienic environments and are processed in a condition fir for human consumption."), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 96,
      columnNumber: 21
    }
  }, "Eko Foodmart is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes. We ensure that consumers obtain their products from hygienic environments and are processed in a condition fir for human consumption."), __jsx("p", {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 97,
      columnNumber: 21
    }
  }, "Eko Foodmart is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes. We ensure that consumers obtain their products from hygienic environments and are processed in a condition fir for human consumption.")))));
};

/* harmony default export */ __webpack_exports__["default"] = (ScrollSpy);

/***/ })

})
//# sourceMappingURL=Legal.js.ddc6b43cf022d5282e75.hot-update.js.map