import Scrollspy from 'react-scrollspy'
 
const ScrollSpy = (props)=>(
    <div className="spy_grid">
       <div>
            <div className="scroll_links">
                <h4 className="title">Legal</h4>
                <Scrollspy items={ ['terms_of_use','privacy_policy'] } currentClassName="is-current" offset={ -150 }>
                <li><a href="#terms_of_use">Terms of Use</a></li>
                    <li><a href="#privacy_policy">Privacy policy</a></li>
                   

                </Scrollspy>
            </div>
       </div>
        <div className="scroll_containers">
        <section id="terms_of_use"> 
                <h4>Terms of Use</h4>
                {/* <span>Last updated 4/04/2020</span> */}
                <div>
                    <p>Welcome to Farmcrowdy Foods (<span>the “Platform”</span>), a digitized food and commodities market which provides users convenient shopping for good quality and affordable food stuff and commodities. </p>
                    <ol type="1">
                        <li>
                            <h6>Consent</h6>
                            <p className="first_p">This Terms of Use constitutes a legally binding contract between you, (whether as a guest or registered user) and Farmcrowdy Foods in your access and use of the Platform as well as its content, services and functionality made available on or through the Platform (collectively referred to as <span>“the Services”</span>). We therefore kindly request that you carefully read through these Terms and Conditions of Use (the “Terms of Use” or “User Agreement”) before using the Platform and click on the <span className="h6">Agree</span> icon if you consent and agree to be bound by (1) this Terms of Use and (2) the Privacy Policy (“the Policy”).</p>
                            <p>By using the Platform, you accept the terms and conditions contained on this Platform, which shall govern your use of this Platform, including all pages or links on the Platform. If you do not agree to these Terms of Use or the Privacy Policy, please do not use the Platform and/or the Services and exit immediately. In addition, if you do not agree to these Terms of Use or the Policy, you agree that your sole and exclusive remedy is to discontinue using this Platform. That notwithstanding, if you proceed to browsing, accessing, or otherwise using the Services in any manner, it will be deemed that you have accepted, without limitation or qualification, both the Terms of Use and the Privacy Policy for the Platform (the <span>“Policy”</span>).</p>
                        </li>
                        <li>
                            <h6>Review of Terms of Use</h6>
                            <p className="first_p">Farmcrowdy Foods <span>(“We” “Us”)</span> may, at our sole discretion, review, revise and/or update this Terms of Use and the Platform at any time. We shall notify You of any modifications and/or updates to these Terms of Use which shall become effective immediately upon the posting thereof or on any specified date. We therefore advise that you read through this Terms of Use periodically. The most current version of the Terms of Use can be accessed at any time by selecting Terms of Use link on the bottom of the home page for the Platform.</p>
                        </li>
                        <li>
                            <h6>Age</h6>
                            <p className="first_p">You must be at least 18 (eighteen) years old to use our Platform or any of our Services; by using our Platform or agreeing to these terms and conditions, you represent and warrant to us that you are at least 18 years of age. Individuals under the age of 18, or applicable age of maturity, may utilize our Services only with involvement of a parent or legal guardian, under such person’s account. </p>
                        </li>
                        <li>
                            <h6>Security of Password</h6>
                            <p className="first_p">To fully enjoy our Services, you may be required to create an account and in which case you will be given (or you may provide) a username and password. Certain areas of our Services are only accessible with the use of username and passwords (“User Restricted Areas”). Please note that you are fully and solely responsible for any and all use of the Services and such, you are required to keep your password secret at all times. Do not share your password with any third party or allow another person to access the User Restricted Areas using your password. Kindly notify us immediately if you have any reason to believe that your username or password has been lost or compromised or misused in any way. Also immediately report any unauthorized or suspicious activity in your account.</p>
                        </li>
                        <li>
                            <h6>Available Content and Use</h6>
                            <ol type="a">
                                <li>
                                    <span>Content Description:</span>
                                    <p className="first_p">The Platform contains variety of contents, including but not limited to text, data, files, documents, software, scripts, layout, design, function, aesthetics, graphics, images, audio, videos, audiovisual combinations, interactive features and any other materials that you may view, access or download (but only as expressly permitted in paragraph C below) on the Platform.</p>
                                </li>
                                <li>
                                    <span>Proprietary Rights</span>
                                    <p className="first_p">All the contents of the Platform, whether publicly posted or privately transmitted, as well as all derivative works thereof, are owned by Farmcrowdy Foods, its affiliates and where applicable, to third parties who have licensed such rights to Farmcrowdy Foods <span>(“Licensors”)</span>. Hence, the Platform is protected by copyright, trademark, and other applicable intellectual property rights/laws. Except as specifically permitted herein, you shall not copy, reproduce, republish, upload, post, transmit, modify or distribute the Platform or any part of it in any way, including by e-mail or other electronic means, without the prior consent of Farmcrowdy Foods or its Licensors.</p>
                                </li>
                                <li>
                                    <span>Restrictions on use of the Platform</span>
                                    <p className="first_p">Please note that the contents of this Platform are solely for your information and personal use, as intended through the provided functionality of the Services and permitted under this User Agreement. As such, you:</p>
                                    <ol type="i">
                                        <li>are prohibited from modifying or using the Content on any web Platform or networked computer environment or using the Content for any purpose other than personal, non-commercial use, without the consent of Farmcrowdy Foods or its licensors first had and obtained, where applicable. Such use or modification is a violation of the copyright, trademark, and other proprietary rights in the Content.</li>

                                        <li>shall not use our Platform in any way or take any action that causes or may cause damage to the Platform or impairment of the performance, availability or accessibility of the Platform;</li>

                                        <li>shall not use our Platform in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity or in any way that breaches any applicable law or regulations;</li>

                                        <li>shall not circumvent, disable or otherwise interfere with security-related features of the Services; including security features that prevent or restrict the use or copying of any content.</li>

                                        <li>shall not use data collected from our Platform for any direct marketing activity (including without limitation email marketing, SMS marketing, telemarketing or direct mailing).</li>

                                        <li>shall not alter, remove, or falsify any attributions or other proprietary designations of origin or source of any Content appearing on the Platform.</li>

                                        <li>Shall not use the Platform in any way to create liability, or in any way that causes us to lose the services of our Internet Service Providers or other suppliers. </li>

                                        <li>shall not download any content on the Platform unless you see a “download” or similar link displayed by us on the Platform for such content. </li>
                                    </ol>
                                    <p>You further agree that: </p>
                                    <ol type="i">
                                        <li>you are responsible for your own conduct while using the Platform or Services and for any consequences thereof;</li>

                                        <li>You shall use this Platform and all Services on the Platform only for purposes that are legal, proper and in accordance with this Terms of Use, the Privacy Policy, and any applicable law, rules or regulations, any consumer protection, unfair competition, and anti-discrimination laws or regulations and any applicable foreign laws).</li>

                                        <li>You are solely responsible for your interactions with other users of the Platform (“Users”) both within the Platform and outside of the Platform. Farmcrowdy Foods expressly disclaims any responsibility for interactions, loss or liabilities between users or between you and any third party outside of the Platform. </li>
                                    </ol>
                                </li>
                                <li>
                                    <span>Disclaimer</span>
                                    <ol type="i">
                                        <li>
                                            <p>Your use of the Services is at your sole risk and the Platform is provided to you on an ‘AS IS’ and ‘AS AVAILABLE’ basis. You therefore agree to evaluate, seek independent advice and bear all risks associated with the use of the Platform.</p>
                                            <p>Nothing on the Platform should be deemed to constitute a recommendation to purchase, sell or hold, or otherwise to constitute an advice regarding, any investment, secured or otherwise.</p>
                                        </li>

                                        <li>We will not be liable for any damages (including, without limitation, damages for any consequential loss or loss of business opportunities or projects, or loss of profits) howsoever arising and whether in contract, tort or otherwise from the use of or inability to use our Platform, or any of its contents and materials, or from any action or omission taken as a result of using the Platform or any such contents. In any event, our liability for any and all proven damages or losses shall not in any circumstances exceed the amount paid by you, if any, for accessing this Platform.</li>

                                        <li>All liability is excluded to the fullest extent permitted by applicable law including any implied terms as the content of this Platform “as is” without warranties of any kind. We reserve the right to change all the contents of this Platform at any time without notice to you.</li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <h6>Confidentiality</h6>
                            <ol type="a">
                                <li>You undertake that all communication, content, intellectual property or other information, and materials on the Platform, either marked ‘confidential’ or is by its nature intended to be for your knowledge alone, shall be kept confidential unless or until you can reasonably demonstrate that such communication, information and material is, or part of it is, in the public domain through no fault of yours. Furthermore, any communication, content, intellectual property or other information, and materials you obtain in terms of or arising from the use of this Platform shall be treated as confidential and shall not be divulged or permitted to be divulged to third parties, without our prior written consent.</li>

                                <li>Please note that all obligations relating to Confidential Information under this Terms of Use will continue after termination of the Terms of Use and termination of access rights hereunder.</li>
                            </ol>
                        </li>
                        <li>
                            <h6>Use of suggestions</h6> 
                            <p className="first_p">You acknowledge and agree that any questions, comments, suggestions, ideas, feedback, or other information provided by you to Farmcrowdy Foods (collectively, “Feedback”) are not confidential and you hereby grant to Farmcrowdy Foods a worldwide, perpetual, irrevocable, royalty-free license to reproduce, display, perform, distribute, publish, modify, edit or otherwise use such Feedback as it deems appropriate, for any and all commercial or non-commercial purposes, in its sole discretion.</p>
                        </li>
                        <li>
                            <h6>Undertakings</h6> 
                            <p className="first_p">You hereby agree as follows:</p>
                            <ol type="a">
                                <li>not to access or use the Services in an unlawful way or for an unlawful or illegitimate purpose, including without limitation any violation of any criminal or anti-money laundering laws of the Federal Republic of Nigeria or any other jurisdiction where you are resident;</li>

                                <li>transmit on the Platform (a) any message or information under a false name; (b) information that is unlawful, malicious, libelous, defamatory, obscene, fraudulent, predatory of minors, harassing, discriminatory, threatening or hateful to any person; or (c) information that infringes or violates any contractual or intellectual property rights of others or the privacy or publicity rights of others;</li>

                                <li>not to transmit, distribute, introduce or otherwise make available in any manner through the Services any computer virus, keyloggers, malware, spyware, worms, Trojan horses, time bombs or other malicious or harmful code (collectively, “Harmful Code”) or to do any act capable of disrupting the operation of the Platform; Please note that we do not have an obligation to detect the presence of Harmful Code (as defined in “Restrictions on the use of your Services” below. Please note that if you download any Content from the Platform, you do so at your own risk. </li>

                                <li>not to use the Platform in any manner that could damage, disable or impair our services or networks;</li>

                                <li>not to attempt to gain unauthorized access to the Platform or any user accounts or computer systems or networks, through hacking, password mining or any other unlawful means;</li>
                            </ol>
                        </li>
                        <li>
                            <h6>Intellectual Property</h6> 
                            <ol type="a">
                                <li>Farmcrowdy Foods shall remain the owner of the know-how, trademarks, service marks, logos, slogans, patents, copyright or other intellectual property rights belonging to it within or outside the Platform. The use of the Platform, and nothing contained herein shall create nor transfer any intellectual property right belonging to Farmcrowdy Foods, and you are not allowed to use any such rights belonging to Farmcrowdy Foods without our written consent.</li>

                                <li>If you believe that any content on the Platform infringes upon your copyrights, please contact us through the contact details below. </li>
                            </ol>
                        </li>
                        <li>
                            <h6>Financial Partners</h6> 
                            <p className="first_p">At Farmcrowdy Foods, we partner with other financial/payment service providers in providing our Services to you. In order to enable you make payments on/through the Platform, we may share your information with these financial partners. You therefore authorize us to share your identity and banking information with partner financial institutions in respect of our Services. Farmcrowdy Foods shall not be liable for any default, negligence or breach of the financial partners. </p>
                        </li>
                        <li>
                            <h6>Termination/Suspension</h6> 
                            <p className="first_p">In the event of any breach of this Terms of Use, Farmcrowdy Foods shall have the right to suspend or terminate all or a portion of the Services to you in our discretion, or restrict your access to the Platform. We reserve the right to revoke, terminate or suspend any privileges associated with accessing the Services for any reason (including regulatory instruction) or for no reason whatsoever. You agree that Farmcrowdy Foods shall not be liable to you or any third party for any termination or suspension of your access to the Platform.</p>
                        </li>
                        <li>
                            <h6>Links to other Platforms</h6> 
                            <p className="first_p">The Platform contain links to other platforms/websites or resources on the internet. When you access third party platforms/websites, you do so at your own risk. Links to other platforms/websites do not constitute an endorsement, control, guarantee, authorization or representation of our affiliation with those third parties. Kindly note that this Terms of Use or our Privacy Policy do not apply to those third-party Platforms, as such, you agree that Farmcrowdy Foods shall not be liable under any circumstances in relation to your use of such other platforms/websites.</p>
                        </li>
                        <li>
                            <h6>Disclaimer of Warranty</h6> 
                            <ol type="a">
                                <li>The Platform, its content and all services on the platform are provided “as is” and “as available” without any warranty of any kind, express or implied. To the fullest extent permissible pursuant to applicable law, Farmcrowdy Foods, its affiliates and any person associated with Farmcrowdy Foods and its affiliates, disclaims all warranties of any kind, either express or implied, statutory or otherwise, including but not limited to any implied warranties of title, merchantability, fitness for a particular purpose or non-infringement. </li> 

                                <li>Without limiting the foregoing, Farmcrowdy Foods does not warrant that access to the Platform will be uninterrupted or error-free, or that defects, if any, will be corrected within any specific time frame; nor does Farmcrowdy Foods, its affiliates, nor any person associated with Farmcrowdy Foods make any representations about the accuracy, standard, reliability, currency, quality, completeness, usefulness, performance, security, or suitability of the Platform. Farmcrowdy Foods does not warrant that the Platform is free of viruses or other harmful components. You shall be solely and fully responsible for any damage to any computer system, any loss of data, or any improper use or improper disclosure of information caused by you or any person using your login credentials. Farmcrowdy Foods cannot and does not assume any responsibility for any loss, damages or liabilities arising from the failure of any telecommunications infrastructure, or the internet or for your misuse of any of advice, ideas, information, instructions or guidelines accessed through the Services. It is your responsibility to install or download sufficient software or hardware protection for your device or computer. </li>
                            </ol>
                        </li>
                        <li>
                            <h6>Limitation of liability</h6> 
                            <ol type="a">
                                <li>Farmcrowdy Foods and its subsidiaries, affiliates, officers, directors, agents or employees are not liable to you or any other person for any punitive, exemplary, consequential, incidental, indirect or special damages (including, without limitation, any personal injury, lost profits, business interruption, loss of programs or other data on your computer or otherwise) arising from or in connection with your use of the Platform, whether under a theory of breach of contract, tort, negligence, strict liability, malpractice, any other legal or equitable theory or otherwise, even if Farmcrowdy Foods has been advised of the possibility of such damages. You hereby release Farmcrowdy Foods and hold it and its parents, subsidiaries, affiliates, officers, directors, agents and employees harmless from any and all claims, demands, and damages of every kind and nature (including, without limitation, actual, special, incidental and consequential), known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way connected with your use of the Services. You waive the provisions of any state or local law limiting or prohibiting a general release.</li>

                                <li>The foregoing limitation of liability shall apply to the fullest extent permitted by law in the applicable jurisdiction. This limitation of liability also applies with respect to damages incurred by you by reason of any products or services sold or provided on any linked Platforms or otherwise by third parties other than Farmcrowdy Foods and received through the Services or any linked Platforms.</li>

                                <li>Without prejudice to the limitation of liability above, in the event of any problem with the Platform or any of the services on the Platform, you agree that your sole and exclusive remedy is to cease using the Platform. Under no circumstances shall Farmcrowdy Foods, its subsidiaries, affiliates, officers, directors, agents or employees be liable in any way for your use of Services, including, but not limited to, any errors or omissions in the Platform, downtime, any infringement by the Platform of the intellectual property rights or other rights of third parties, or for any loss or damage of any kind incurred as a result of the use of the Services.</li>
                            </ol>
                        </li>
                        <li>
                            <h6>Breaches of Terms and Conditions</h6> 
                            <ol type="a">
                                <li>
                                Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we shall: 
                                    <ol type="i">
                                        <li>send you one or more formal warnings in writing to this effect;</li>

                                        <li>temporarily suspend your access to our Platform;</li>

                                        <li>permanently prohibit you from accessing our Platform;</li>

                                        <li>block your access to the Platform;</li>

                                        <li>commence legal action against you, whether for breach of contract or otherwise; and/or</li>

                                        <li>suspend or delete your account on our Platform.</li>
                                    </ol>
                                </li>
                                <li>Where we suspend or prohibit or block your access to our Platform or a part of our Platform, you must not take any action to circumvent such suspension or prohibition or blocking (including without limitation, creating and/or using a different account).</li>
                            </ol>
                        </li>
                        <li>
                            <h6>Indemnification</h6> 
                            <ol type="a">
                                <li>Without limiting the generality or effect of other provisions of this Terms of Use, you agree to indemnify, hold harmless, and defend Farmcrowdy Foods and its subsidiaries, affiliates, officers, directors, affiliates, agents and employees, parent companies, (collectively, “Indemnified Parties” and each, individually, an “Indemnified Party”) against all costs, expenses, liabilities and damages (including reasonable attorney’s fees) incurred by any Indemnified Party in connection with any third party claims arising out of: (i) your use of the Platform or any part of it; (ii) your failure to comply with any applicable laws and regulations; and (iii) your breach of any obligations set forth in this Terms of Use. </li>

                                <li>This indemnity obligation shall survive this Terms of Use and your use of the Platform. Farmcrowdy Foods reserves the right to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will assist and cooperate with Farmcrowdy Foods in asserting any available defenses.</li>
                            </ol>
                        </li>
                        <li>
                            <h6>Governing law and dispute resolution </h6> 
                            <ol type="a">
                                <li>This Terms of Use and all disputes and matters arising from the Platform (or its use) shall be governed by the laws of the Federal Republic of Nigeria.</li>
                                <li>In the event of a controversy, claim or dispute arising out of or relating to this Terms of Use, the Parties shall attempt in good faith to resolve such controversy, claim or dispute promptly by negotiation between the parties or their authorized representatives. You shall, before exploring any other remedy in law, provide Farmcrowdy Foods at least 30- days notification of the dispute or complaint in writing through the contact details below. If parties are unable to resolve the controversy, claim or dispute, the parties shall be at liberty to explore any other dispute resolution mechanism known to Law including mediation, arbitration or litigation. In the course of dispute resolution, each party shall bear its own cost of dispute resolution.</li>
                            </ol>
                        </li>
                        <li>
                            <h6>Miscellaneous</h6> 
                            <ol type="a">
                                <li><span>Entire Agreement:</span> This User Agreement constitutes the sole agreement between you and Farmcrowdy Foods for your use and the provision of the Services and the subject matter hereof, and no representations, statements or inducements, oral or written, not contained in this Terms of Use shall bind either you or Farmcrowdy Foods.</li>

                                <li><span>Severance: </span>If any provision of this Terms of Use is prohibited by law or judged by a court to be unlawful, void or unenforceable, the provision shall, to the extent required, be severed from this Terms of Use and rendered ineffective as far as possible without modifying the remaining provisions of this Terms of Use, and shall not in any way affect any other circumstances of or the validity or enforcement of this Terms of Use.</li>

                                <li><span>Waiver:</span> Failure to insist on performance of any of the terms of this Terms of Use will not operate as a waiver of any subsequent default. No waiver by Farmcrowdy Foods of any right under this Terms of Use will be deemed to be either a waiver of any other right or provision or a waiver of that same right or provision at any other time.</li>

                                <li><span>Assignment:</span> You may not assign, transfer or delegate your rights or obligations hereunder, in whole or in part.</li>

                                <li><span>Bindingness:</span> This Terms of Use shall be binding upon and inure to the benefit of each of the parties and the parties’ respective successors and permitted assigns. A printed version of this Terms of Use and of any related notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this Terms of Use to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form.</li>
                            </ol>
                        </li>
                        <li>
                            <h6>Notice/Contact Details</h6> 
                            <p className="first_p">In the case of any complaints or notice of dispute or any other notification, please contact us at:</p>
                            <p className="first_p mb_0">No. 2 Opeyemisi Bamidele Crescent, Off Freedom Way, Lekki Phase 1, Lagos, Nigeria.</p>
                            <p className="first_p mb_0"> <span className="h6">Email:</span> <a href="mailto:orders@farmcrowdy.com" target="_blank" rel="noopener noreferrer">orders@farmcrowdy.com</a></p>
                            {/* <p className="first_p"> <span className="h6">Phone number:</span> <a href="tel:+2347037468275" target="_blank" rel="noopener noreferrer">+2347037468275</a></p> */}
                    
                        </li>
                    </ol>
                </div>
            </section>
           
            <section id="privacy_policy">
                <h4>Privacy policy</h4>
                {/* <span>Last updated 4/04/2020</span> */}
                <div>
                    <h6 className="h6">Introduction</h6>
                    <p className="first_p">The Farmcrowdy Foods (<span className="span">“Farmcrowdy Foods”, “we”, “us”, “our”</span>), recognizes your privacy rights as guaranteed under the 1999 Constitution of the Federal Republic of Nigeria; The Nigerian Data Protection Regulation, and other applicable privacy laws in Nigeria. Thus, it is important to us as a law-abiding organisation that your Personal Data is managed, processed and protected in accordance with the provisions of the applicable laws. In the course of our business and/or your engagement with us and third parties through our Platforms (this includes but are not limited to our websites, digital platforms, mobile applications, physical operations/offices, amongst others), we may process your Personal Data, subject however to the terms of this Policy. This Privacy Policy (“Policy”) therefore explains our privacy practices with respect to how we collect, process your Personal Data and describes your rights as a user of any of our services and Platforms. </p>
                    <p>This Policy applies to all our Platforms, and all related sites, applications, services and tools regardless of how they are accessed or used. In this Privacy Policy, “we”, “us” or “our” refers to the Farmcrowdy Foods its successors, agents and assigns, while “you”, “your” refers to you and/or any person who subscribes for, uses or authorizes the use of the Service.</p>
                    <p>As a user of any of our Platforms, you accept this Privacy Policy when you sign up for, or use our products, services, content, technologies or functions offered on our Platforms and all related sites and services (<span className="span">“Services”</span>).</p>
                    <p className="sub_h">Our Privacy Policy explains our user’s privacy rights regarding:</p>
                    <ol type="a">
                        <li>What information we collect</li>
                        <li>Why we collect such information</li>
                        <li>The use of such information</li>
                        <li>The storage, sharing and protection of such information</li>
                    </ol>
                    <h6 className="h6">Definitions</h6>
                    <p className="first_p">For the purpose of this Policy:</p>
                    <p><span className="h6">Personal Data/Information</span> means information relating to you, including your name, location data, online identifier address, email address, pins, passwords, bank details, and other unique identifiers such as but not limited to IP address, IMEI number, Bank Verification Number, IMSI Number, SIM and others. </p>
                    <p><span className="h6">Process/Processing</span> means any operation or set of operations which is performed on your Personal Data or sets of Personal Data, whether or not by automated means, such as collection, recording, organisation, structuring, storage, adaptation or alteration, retrieval, consultation, use, disclosure by transmission, dissemination or otherwise making available, alignment or combination, restriction, erasure or destruction.</p>
                    <h6 className="h6">Consent</h6>
                    <p className="first_p">We kindly request that you carefully read through this Policy and click on the <span className="h6">‘Agree’</span> icon below, if you consent to the terms of this Policy, particularly with respect to the processing of your Personal Data. That notwithstanding, if you proceed to use any of our Platforms and services, it will deemed that you have provided your express consent to the terms of this Policy. </p>
                    <h6>Age</h6>
                    <p className="first_p">You must be at least 18 years old to use our services or any of our Platforms. Individuals under the age of 18, or applicable age of maturity, may utilize our Platforms services only with involvement of a parent or legal guardian, under such person’s account. Regardless, all Personal Data which we may process shall be in accordance with this Policy and other applicable laws. </p>
                    <h6>Collection of Personal Data</h6>
                    <p>In the course of your engagement with us or with third parties through our Platforms, we may collect your Personal Data in the following ways: </p>
                    <ul className="li_space">
                        <li><span className="h6">Automatic Information collection:</span> We may automatically collect Information upon your engagement with us or our Platforms through your computer, mobile phones and other access devices. The information sent includes, but is not limited to, data about our pages you access, geo-location information; statistics on page views; traffic to and from our Platforms. </li>
                        <li><span className="h6">Information from downloads:</span> When you download or use our digital Platforms or access our site, we may receive non personal information from your mobile device about your location, number of downloads, geo location of downloads. You may disable your location in the settings menu of the device. </li>
                        <li><span className="h6">Physically or Digitally Provided Information:</span> We may collect and process your information when you create and/or update your account on our Platform; complete forms, questionnaires, surveys etc. When you use our Services, we may also collect information about your transactions and your activities. In addition, when you open an account on our Platform, we may collect information such as your contact information, which includes your name, address, phone number, email or other similar information; financial information such as your full bank account numbers and/or credit card or debit card numbers. </li>
                        <li><span className="h6">Third Parties:</span> We may also receive your information from third parties such as, guardians, financial institutions, identity verification services, vendors, and service providers etc. </li>
                        <li><span className="h6">Social Media:</span>  We also receive Information through your engagements with us on social media sites (e.g., Facebook, Instagram, LinkedIn, Twitter, etc). This includes but are not limited to your replies to our posts, your comments, enquiries, messages to us, etc. We may also collect information from your public profile and updates on social media.  </li>
                    </ul>
                    <h6>Accessing your Personal Information</h6>
                    <p>When accessing our Platforms, we may collect information about you and your interactions with the Services to which we undertake to keep secure and confidential.</p>
                    <p>We may offer you the ability to connect with our Platforms using a mobile device, either through a mobile application (App), computer, mobile optimized website, or by any other means. The provisions of this Privacy Policy apply to all such mobile access and use of mobile devices.</p>
                    <p>When you download or use our mobile applications, or access one of our mobile optimized sites, we may receive information about your location and your mobile device, including a unique identifier for your device. We may use this information to provide you with location-based services, such as advertising, search results, and other personalized content. Most mobile devices allow you to control or disable location services in the device's setting's menu. If you have questions about how to disable your device’s location services, we recommend you contact your mobile service carrier or the manufacture of your particular device.</p>
                    <h6>Using Your Personal Information</h6>
                    <p>In the course of your engagements with us or through our Platforms, we collect personal information for various legal reasons, largely to enable us to personalize your experience and to provide a more efficient service to you. Some of the reasons we collect Information are to:</p>
                    <ul>
                        <li>provide services and customer support;</li>
                        <li>process transactions, payment requests, and send notices about transactions; </li>
                        <li>create an account with us for the provision or use of our services;</li>
                        <li>communicate with you about your account or transactions with us and send you information or request feedback about features on our website and applications or changes to our policies;</li>
                        <li>send you periodic emails and updates pertaining to our products and services;</li>
                        <li>verify customers’ identity, including during account creation and password reset processes; </li>
                        <li>manage your account and provide you with efficient customer service,</li>
                        <li>send you offers and promotions for our services and investment opportunities</li>
                        <li>resolve disputes, process payments and troubleshoot problems; </li>
                        <li>detect, investigate and prevent activities that may violate our policies or be illegal;</li>
                        <li>manage risks, or to detect, prevent, and/or remediate fraud, violation of policies and applicable user agreements or other potentially prohibited or illegal activities;</li>
                        <li>execute our contractual obligations to you; </li>
                        <li>improve our services and functionality by customizing user experience; </li>
                        <li>measure the performance of our services and improve their content and layout; </li>
                        <li>manage and protect our information technology infrastructure; </li>
                        <li>provide targeted marketing and advertising, provide service or transaction update notices, and deliver promotional offers based on communication preferences; </li>
                        <li>obtain a means by which we may contact you; either by telephone, text (SMS), email messaging, social media, etc; </li>
                        <li>conduct background checks, compare information for accuracy and verify same with third parties;</li>
                        <li>identify or address a violation and administer our policies and terms of use;</li>
                        <li>comply with legal, contractual and regulatory obligations;</li>
                        <li>execute your specific requests or use same for a specific purpose as you may instruct;</li>
                        <li>investigate and respond to your complaints or enquiries;</li>
                        <li>process your access to our services, Platforms, or functions from time to time;  </li>
                    </ul>
                    <p>If we intend to use any Personal Information in any manner that is not consistent with this Privacy Policy, you will be informed of such anticipated use prior to or at the time at which the Personal information is required and obtain your consent.</p>
                    <h6>Storage and protection of your Data</h6>
                    <p>We protect your personal Information using physical, technical, and administrative security measures to reduce the risks of loss, misuse, unauthorized access, unauthorised disclosure and alteration. Some of our safeguards include firewall protection, Virtual Private Network (VPN) protection, encryption of data, authorization keys for authorised access and strong passwords. Access to our various platforms is restricted to authorized users only. Your Information is also stored on our secure servers as well as secure physical locations and cloud infrastructure (where applicable).   Please be aware that, despite our best efforts, no security measures are perfect or impenetrable. We will retain your personal information for the length of time needed to fulfil the purposes outlined in this privacy policy unless a longer retention period is required or permitted by law. To dispose of personal data, we may anonymize it, delete it or take other appropriate steps. Data may persist in copies made for backup and business continuity purposes for additional time.</p>
                    <p>We will take all necessary measures to ensure that your personal Data is safe, however, you are also required to ensure that access codes, PINs, passwords, usernames, and all other information or hints that may enable third party access to your accounts on our Platforms are secure. We therefore strongly advise you to keep such information secure and confidential. If you use a third party’s device (laptops, phones, public internet, etc.) to access your account, kindly ensure that you always log out. Kindly note however that certain devices are programmed to save passwords or usernames, as such, we advise that you use third party devices with extreme caution. If you believe that an unauthorized person has accessed your information, please contact us immediately.</p>
                    <h6>Processing your Information </h6>
                    <p>In order to execute our obligations to you or process your transactions, we may be required to process your Information, such as your name, account number, account ID, contact details, shipping and billing address, or other information needed to complete the transaction. We also work with third parties, including financial institutions, vendors, service providers, who at one point or the other facilitate transactions executed on our Platforms. For completeness, in the course of your engagement with us or use of our services and Platforms, we may share your information with different stakeholders, including but not limited to Financial institutions; service providers; Credit bureaus and collection agencies to report account or credit information; our subsidiaries and affiliates; Regulatory or judicial authorities; or other third parties pursuant to a subpoena, court order, or other legal process or applicable requirement. Please note that the aforementioned parties may be within or outside Nigeria. </p>
                    <p>We may also process your Information when we believe, in our sole discretion, that the disclosure of your Information is necessary to comply with applicable laws and judicial/regulatory orders; conduct investigations; manage existing or imminent risks, prevent fraud, crime or financial loss, or for public safety or to report suspected illegal activity or to investigate violations of our Terms and Conditions.</p>
                    <p>In all cases, we will ensure that your Information is safe, and notify the receiving party of the confidential nature of your Information, particularly the need to maintain the confidentiality of same and prevent unlawful or unauthorised usage.</p>

                    <h6>Your Rights</h6>
                    <p>You have the following rights regarding your personal information collected by us:</p>
                    <ul>
                        <li>Right to access your personal information being held by us. Request for such information may be sent to <a href="mailto:orders@farmcrowdy.com" target="_blank" rel="noopener noreferrer">orders@farmcrowdy.com</a>.</li>
                        <li>Right to request that your personal data be made available to you in an electronic format or that it should be sent to a third party (Kindly note that we have the right to decline such request if it is too frequent, unreasonable, and likely to cause substantial cost to us. In any event, we will communicate the reason for our refusal);</li>
                        <li>Right to rectify any inaccurate, incomplete information. As such, if you discover any inaccuracy in your personal information, kindly notify us promptly and provide us with documentary evidence to enable us to update the requisite changes.</li>
                        <li>Right to withdraw consent for the processing of your information, provided that such withdrawal shall not invalidate any processing hitherto done based on the consent previously given by you;</li>
                        <li>Restrict or object to the processing of your personal data provided that we may be compelled to process your data where required under law, regulatory authorities, or court of law.</li>
                        <li>Right to request that your personal data be deleted. We may however continue to retain the information where required under law, contract, regulatory authorities, or court of law.</li>

                    </ul>
                    <h6>Disclosures </h6>
                    <p>We may share your personal information with:</p>
                    <ol type="a">
                        <li>Members of the Farmcrowdy Foods to provide joint content, products and services (such as registration, transactions and customer support), to help detect and prevent potentially illegal acts and violations of our policies, and to guide decisions about their products, services, and communications. Members of the group will use this information to send you marketing communications and opportunities only if you have requested their services.</li>

                        <li>Credit bureaus and collection agencies to report account or credit information, as permitted by law.</li>

                        <li>Companies that we plan to merge with or are acquired by. (Should such a combination occur, we will require that the new combined entity follow this Privacy Policy with respect to your personal Information. If your personal Information could be used contrary to this policy, you will receive prior notice).</li>

                        <li>Law enforcement, government officials, or other third parties pursuant to a subpoena, court order, or other legal process or requirement applicable to Farmcrowdy Foods or one of its agents; when we need to do so to comply with law; or when we believe, in our sole discretion, that the disclosure of personal information is necessary to prevent physical harm or financial loss, to report suspected illegal activity or to investigate violations of our User Agreement.</li>

                    </ol>
                    <p>Other unaffiliated third parties, for the following purposes</p>
                    <ol type="a">
                        <li>Fraud Prevention and Risk Management: to help prevent fraud or assess and manage risk.</li>

                        <li>Customer Service: for customer service purposes, including to help service your accounts or resolve disputes.</li>

                        <li>Legal Compliance: to help them comply with anti-money laundering and counter-terrorist financing verification requirements.</li>

                        <li>Service Providers: to enable service providers under contract with us to support our business operations, such as fraud prevention, marketing, customer service and technology services. Our contracts dictate that these service providers only use your information in connection with the services they perform for us and not for their own benefit.</li>
                    </ol>
                    <h6>Software</h6>
                    <p>If you download or use our software, such as a stand-alone software product, an app, or a browser plugin, you agree that from time to time, the software may download and install upgrades, updates and additional features from us in order to improve, enhance, and further develop the software. We may utilise your personal information to internally evaluate our software.</p>
                    <h6>Period of Storage</h6>
                    <p>We retain your information for as long as necessary for the purpose(s) for which it was collected. The period of data storage is also subject to legal, regulatory, administrative and operational requirements. </p>

                    <h6>Exceptions</h6>
                    <p>Please note that this Policy does not apply to Information that is already in the public domain through no fault of ours. </p>
                    <h6>Violation</h6>
                    <p>If you violate the letter or spirit of this Policy, or otherwise create risk or possible legal exposure for us or attempt to violate the privacy rights of Farmcrowdy Foods and/or its other users, we reserve the right to restrict your access to our Platforms. We will notify you in the event that we are constrained to take such decision.</p>
                    <h6>Dispute resolution </h6>
                    <p>We are dedicated to ensuring that you are satisfied with our management of your Information. However, in the unlikely event that you have a complaint, please contact us via the details below, stating your name and details of your complaint. Upon receipt of your complaint we will endeavor to contact you within 3 (three) working days with a view to amicably resolving such dispute within the shortest possible time. The foregoing notwithstanding, all disputes arising from this policy shall first be resolved by negotiation. However, if parties fail to resolve the disputes amicably by such mutual consultations, parties shall further attempt to resolve the dispute by mediation.</p>
                    <h6>Contact Us </h6>
                    <p>If you have questions regarding your data privacy rights or would like to submit a related data privacy right request, kindly contact us via the information below: </p>
                    <p className="first_p mb_0">No. 2 Opeyemisi Bamidele Crescent, Off Freedom Way, Lekki Phase 1, Lagos, Nigeria.</p>
                    <p className="first_p mb_0"> <span className="h6">Email:</span> <a href="mailto:orders@farmcrowdy.com" target="_blank" rel="noopener noreferrer">orders@farmcrowdy.com</a></p>
                    <p className="first_p"> <span className="h6">Phone number:</span> <a href="tel:+2347037468275" target="_blank" rel="noopener noreferrer">+2347037468275</a></p>
                    <p>Please allow up to 3 (three) working days for requests to be processed.  We reserve the right to charge a reasonable fee to process excessive or repeated requests.</p>
                    <h6>Amendment</h6>
                    <p>We may amend or revise this Policy at any time by posting a revised version on our website. Notice of the amendment shall be posted on our website and the revised version will be effective from the date of publication. Kindly, note that your continued use of our Platform after the publication of the revised version constitutes your acceptance of our amended terms of the Policy.</p>

                </div>
            </section>
           
        </div>
        
       
        
    </div>
  )

export default ScrollSpy;