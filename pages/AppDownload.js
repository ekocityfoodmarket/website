

const AppDownload= ()=>(
    <div className="app_download_container">
        <div className="section_container">
            <h4 className="title">Download the Farmcrowdy Foods app</h4>
            <p>Start placing your orders.</p>
            <a href="https://play.google.com/store/apps/details?id=com.farmcrowdyfoods" target="_blank" rel="noopener noreferrer"> 
                <img src="/Playstore.svg" alt="Playstore logo"/>
            </a>
            <a href="https://apps.apple.com/us/app/farmcrowdy-foods/id1512137851" target="_blank" rel="noopener noreferrer">
                <img src="/Appstore.svg" alt="Appstore logo"/>
            </a>
        </div>
        <img src="/bg/app_bg.jpg" alt="Fruits"/>
    </div>
)
export default AppDownload;