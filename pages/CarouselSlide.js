import {useEffect, useState} from 'react';
import Carousel from 'react-multi-carousel';
import "react-multi-carousel/lib/styles.css";
 
const CarouselSlide = (props)=>{
  // useEffect(() => {
   
  // }, [input])
  console.log(props.deviceType)
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  }
  return (
    <Carousel 
      swipeable={false}
      draggable={false}
      showDots={false}
      responsive={responsive}
      ssr={true} // means to render carousel on server-side.
      infinite={true}
      autoPlay={props.deviceType !== "mobile" ? true : false}
      autoPlaySpeed={1000}
      keyBoardControl={true}
      customTransition="all .5"
      transitionDuration={5000}
      containerClass="carousel-container"
      removeArrowOnDeviceType={["tablet", "mobile", "desktop"]}
      deviceType={props.deviceType}
      dotListClass="custom-dot-list-style"
      itemClass="carousel-item-padding-40-px"
    >
  <div className="slider_container">
    <div className="slider_bg">
      <img src="why1.svg" alt="Wallet icon"/>
      <h5>Affordable</h5>
      <p>Obtain value for your money by purchasing our healthy food products at a reasonable price.</p>
    </div>
  </div>

  <div className="slider_container">
    <div className="slider_bg">
      <img src="why2.svg" alt="Wallet icon"/>
      <h5>Fresh</h5>
      <p>Purchase fresh, clean, and healthy food products produced and processed under hygienic conditions.</p>
    </div>
  </div>

  <div className="slider_container"> 
    <div className="slider_bg">
      <img src="why3.svg" alt="Wallet icon"/>
      <h5>Convenient</h5>
      <p>Eliminate the hassle of going to market to purchase groceries by purchasing our products online. </p>
    </div>   
  </div>
</Carousel>
  )
}
export default CarouselSlide;