import Head from 'next/head';
import ScrollSpy from './ScrollSpy';
import Navbar from '../components/layout/Navbar';
import  '../sass/ekoFoodmart.scss';
import DownloadOptionB from './DownloadOptionB';
import Footer from './Footer';


 
const Legal = (props)=>(
   
    <div>
         {/* {console.log(props)} */}
        <Head>
        <meta name="description" content="Please read these Terms of Use, which set forth the legally binding terms and conditions for your use of the services (the “Service”) offered by Farmcrowdy Foods."/>
    {/* Open Graph / Facebook */} 
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://foods.farmcrowdy.com/"/>
    <meta property="og:title" content="FarmcrowdyFoods"/>
    <meta property="og:description" content="Please read these Terms of Use, which set forth the legally binding terms and conditions for your use of the services (the “Service”) offered by Farmcrowdy Foods."/>
    <meta property="og:image" content="/meta_img.png"/>

 {/*  Twitter */} 
<meta property="twitter:card" content="summary_large_image"/>
<meta property="twitter:url" content="https://foods.farmcrowdy.com/"/>
<meta property="twitter:title" content="FarmcrowdyFoods"/>
<meta property="twitter:description" content="Please read these Terms of Use, which set forth the legally binding terms and conditions for your use of the services (the “Service”) offered by Farmcrowdy Foods"/>
<meta property="twitter:image" content="/meta_img.png"/>
<meta name="theme-color" content="hsl(159, 22%, 50%)" />

            <title>Legal Farmcrowdy Foods</title>
            <link rel="icon" href="/favicon.ico" />
        </Head>
        <Navbar/> 
       <section className="section_container">
            <ScrollSpy/>
       </section>
       {/* app download v2 */}
           <DownloadOptionB/>
        <Footer/>
    </div>
  )

export default Legal;