
const ProductRange = (props)=>(
    
    <div className="produce_main">
        <h4 className="title">Our Produce Range</h4>
        <div className="produce_grid">
          <div className="product">
            <div className="product_container">
              <img src="/produce1.png" alt="Produce"/>
              <h5>Butchery &amp; Poultry</h5>
            </div>
          </div>
          <div className="product">
            <div className="product_container">
              <img src="/produce2.png" alt="Produce"/>
              <h5>Vegetables</h5>
            </div>
          </div>
          <div className="product">
            <div className="product_container">
              <img src="/produce3.png" alt="Produce"/>
              <h5>Grains &amp; Tubers</h5>

            </div>
          </div>
          <div className="product">
            <div className="product_container">
              <img src="/produce4.png" alt="Produce"/>
              <h5>Fruits</h5>

            </div>
          </div>
        </div>
      </div>
)
export default ProductRange;