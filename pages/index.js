import Head from 'next/head'
import Navbar from '../components/layout/Navbar';
import  '../sass/ekoFoodmart.scss';
import Carousel from './CarouselSlide';
import ProductRange from './ProductRange';
import AppDownload from './AppDownload';
import Footer from './Footer';
import {useEffect, useState} from 'react';

const Index = (props)=>{
const [isLoading, setIsLoading] =useState(true);
// console.log(Carousel);
useEffect(() => {
  setIsLoading(false);
}, [isLoading])
  return(
<div>
  <Head>
  <meta name="description" content="Farmcrowdy Foods is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes."/>
    {/* Open Graph / Facebook */} 
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://foods.farmcrowdy.com/"/>
    <meta property="og:title" content="FarmcrowdyFoods"/>
    <meta property="og:description" content="Farmcrowdy Foods is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes."/>
    <meta property="og:image" content="/meta_img.png"/>

 {/*  Twitter */} 
<meta property="twitter:card" content="summary_large_image"/>
<meta property="twitter:url" content="https://foods.farmcrowdy.com/"/>
<meta property="twitter:title" content="FarmcrowdyFoods"/>
<meta property="twitter:description" content="Farmcrowdy Foods is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes."/>
<meta property="twitter:image" content="/meta_img.png"/>
<meta name="theme-color" content="hsl(159, 22%, 50%)" />

  
    <title>Farmcrowdy Foods</title>
    <link rel="icon" href="/favicon.ico" />
  </Head>
  <Navbar/> 
    <section className="hero_bg">
      <div className="section_container">
        <h2>Buy Quality Fresh Produce <span>from the convenience of your home</span></h2>
        <p>Download and sign up now on iOS and Android</p>
        <div className="app_download">
          <a href="https://play.google.com/store/apps/details?id=com.farmcrowdyfoods" target="_blank" rel="noopener noreferrer"> 
              <img src="/Playstore.svg" alt="Playstore logo"/>
          </a>
          <a href="https://apps.apple.com/us/app/farmcrowdy-foods/id1512137851" target="_blank" rel="noopener noreferrer">
              <img src="/Appstore.svg" alt="Appstore logo"/>
          </a>
        </div>
      </div>
    </section>
    {/* partnership */}
    {/* <section className="section_container">
      <div className="partnership">
        <h6>IN PARTNERSHIP WITH </h6>
        <div className="partners_container">
          <div className="p_logo">
            <a href="https://www.farmcrowdy.com/" target="_blank" rel="noopener noreferrer"> 
              <img src="/FarmcrowdyLogo.svg" alt="Farmcrowdy's logo"/>
            </a>
          </div>
          <div className="p_logo">
            <a href="https://freshfield.co/" target="_blank" rel="noopener noreferrer"> 
              <img src="/freshField.png"   alt="Fresh Field's logo"/>
              <img src="/freshField.png"  srcSet="/freshField2.png 300w, /freshField.png 768w, /freshField3.png 1030w"  alt="Fresh Field's logo"/>
            </a>
          </div>
          <div className="p_logo">
            <a href="https://hillsharvestng.com/" target="_blank" rel="noopener noreferrer"> 
              <img src="/Hills_harvest.png" alt="Hills Harvest's logo"/>
            </a>
          </div>
        </div>
      </div>
    </section> */}
    {/* how it works */}
    <section className="section_container">
      <div className="how_it_works">
        <h4 className="title">How Farmcrowdy Foods Works</h4>
        <p>Farmcrowdy Foods gives you instant access to fresh agricultural products such as fruits, vegetables, meat, and poultry from the convenience of your mobile device all by just following these three steps:</p>
        <div className="download_steps_grid">
          
          <div className="download_step">
            <div className="step_cont">
              <img src="/step1.svg" alt="How it works step"/>
              <h4>Download the app</h4>
              <p>Download the Farmcrowdy Foods app on the Google Playstore or Apple Store and set up your account.</p>
            </div>
          </div>
          <div className="download_step">
            <div className="step_cont">
              <img src="/step2.svg" alt="How it works step"/>
              <h4>Select &amp; Order</h4>
              <p>Select the type &amp; quantity of products you want from multiple options including vegetables, grains, meat and poultry. Input your delivery address and pay.</p>
            </div>
          </div>
          <div className="download_step">
            <div className="step_cont">
              <img src="/step3.svg" alt="How it works step"/>
              <h4>Get your Delivery</h4>
              <p>Your fresh, clean, products will be delivered to your doorstep within a 48 hour period from time of order. The products are packaged and preserved appropriately so it remains fresh for you at delivery.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/*  Produce Range */}
    <section className="section_container">
      <ProductRange/>
    </section>
    <section className="why_us">
      <div className="section_container">
        <h4 className="title">Why choose Farmcrowdy Foods?</h4>
        
        {isLoading ?  "" : <Carousel/>  }
      </div>
    </section>
    {/* app download section  */}
    <section>
      <AppDownload/>
    </section>
    {/* footer secttion */}
    <section>
      <Footer/>
    </section> 
</div>
)}
export default Index;