

const DownloadOptionB = ()=>{
    return(
        <section className="app_underline">
        <div className="app_container section_container">
            <div className="description">
                <p>Order for products by first,</p>
                <p className="download">Downloading the Farmcrowdy Foods app <span>on</span></p>
            </div>
            <div className="download_links">
                <a href="https://play.google.com/store/apps/details?id=com.farmcrowdyfoods" target="_blank" rel="noopener noreferrer"> 
                    <img src="/Playstore.svg" alt="Playstore's logo"/>
                </a>
                <a href="https://apps.apple.com/us/app/farmcrowdy-foods/id1512137851" target="_blank" rel="noopener noreferrer">
                    <img src="/Appstore.svg" alt="Appstore's logo"/>
                </a>
            </div>
        </div>
    </section>
    )
}

export default DownloadOptionB;