import Head from 'next/head'
import Navbar from '../components/layout/Navbar';
import  '../sass/ekoFoodmart.scss';
import Footer from './Footer';
import ProductRange from './ProductRange';
import AppDownload from './AppDownload';
import DownloadOptionB from './DownloadOptionB';
import FAQ from './FAQ';
import  '../sass/ekoFoodmart.scss';


const About = (props)=>(
    
<div>
  <Head>
  <meta name="description" content="Farmcrowdy Foods is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes."/>
    {/* Open Graph / Facebook */} 
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://foods.farmcrowdy.com/"/>
    <meta property="og:title" content="Farmcrowdy Foods"/>
    <meta property="og:description" content="Farmcrowdy Foods is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes."/>
    <meta property="og:image" content="/meta_img.png"/>

 {/*  Twitter */} 
<meta property="twitter:card" content="summary_large_image"/>
<meta property="twitter:url" content="https://foods.farmcrowdy.com/"/>
<meta property="twitter:title" content="Farmcrowdy Foods"/>
<meta property="twitter:description" content="Farmcrowdy Foods is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes."/>
<meta property="twitter:image" content="/meta_img.png"/>
{/* <meta name="theme-color" content="hsla(159, 100%, 22%)" /> */}
<meta name="theme-color" content="hsl(159, 22%, 50%)" />

    <title>About Farmcrowdy Foods</title>
    <link rel="icon" href="/favicon.ico" />
  </Head>
  <Navbar/> 
  {/* hero section  */}
  <section className="hero_bg about_container">
    <div className="section_container">
      <h2>The largest online selection of fresh fruits, <span>vegetables, and meat in Nigeria </span></h2>
    </div>
  </section>
  {/* descrioption section  */}
  <section className="section_container description_cont">
    <div className="description_grid">
      <div className="description_item">
        <div className="text_description">
          <h4 className="title">You deserve healthy, fresh food.</h4>
          <p>You get instant access to fresh agricultural products such as fruits, vegetables, meat, and other products you would find in a market from the convenience of your mobile device and have it all delivered to your doorstep.</p>
        </div>
      </div>
      <div className="description_item">
        <div className="img_cont">
          <img src="/des2.jpg" alt="Fruits"/>
        </div>
      </div>
    </div>
    <div className="description_grid swap_children">
      <div className="description_item">
        <div className="img_cont">
          <img src="/des1.jpg" alt="Fruits"/>
        </div>
      </div>
      <div className="description_item">
        <div className="text_description sub_div">
          <h4 className="title">The best produce at the best prices!</h4>
          <p >Our commitment to you is that you get maximum value for your money by purchasing our healthy food products at prices comparable to those obtainable at Mile-12 and other local markets.</p>
        </div>
      </div>
    </div>
  </section>
  {/* produce range section  */}
  <div className="section_container">
    <ProductRange/>
  </div>
  {/* app download v2 */}
  <DownloadOptionB/>
  {/* app download section */}
  {/* faq section  */}
  <section className="faq_container">
    <div className="section_container">
      <h4 className="title">Frequently Asked Questions</h4>
      <FAQ/>
    </div>
  </section>
  <AppDownload/>
  {/* footer section  */}
    <Footer/>
</div>
)
export default About;