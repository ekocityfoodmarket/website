import React from 'react';
import { Collapse } from 'antd';
import 'antd/dist/antd.css';
import  '../sass/ekoFoodmart.scss';


const FAQ = ()=>{
    const { Panel } = Collapse;

    return(
        <div className="collapsible_con">
            <Collapse accordion defaultActiveKey="0" expandIconPosition="right">
                    <Panel header="What is Farmcrowdy Foods?" >
                        <p>Farmcrowdy Foods is a one-stop digital marketplace that allows consumers to conveniently purchase highly affordable fresh food products from the comfort of their homes. We ensure that consumers obtain their products from hygienic environments and are processed in a condition fir for human consumption.</p>
                    </Panel>
                    <Panel header="How does Farmcrowdy Foods work?" >
                        <p>Customers can sign up and purchase agricultural products such as fruits, vegetables, meat, and other products you would find in a market. Upon selecting their items, they make payment and select a delivery date and the fresh produce will be delivered on the selected date.</p>
                    </Panel>
                    <Panel header="How do I sign up?" >
                        <p>To sign up, kindly follow the steps below:</p>
                        <ol>
                            <li>Download our app; &nbsp;
                                <a href="https://play.google.com/store/apps/details?id=com.farmcrowdyfoods" target="_blank" rel="noopener noreferrer">Playstore</a> &nbsp; <a href="https://apps.apple.com/us/app/farmcrowdy-foods/id1512137851" rel="noopener noreferrer">Appstore</a>
                            </li>
                            <li>Sign up with your email address and phone number.</li>
                            <li>Scan through the latest products by categories.</li>
                            <li>Select the items you want from our array of products.</li>
                            <li>Make payment online. Payment is carried out safely through a third party financial platform.</li>
                            <li>Sit back and relax as your fresh produce is delivered to your doorstep.</li>
                        </ol>
                    </Panel>
                    <Panel header="Will you deliver to my area?" >
                        <p>We currently deliver to a number of locations within Lagos state, except Badagry &amp; Epe Regions. Delivery price is dependent on the area we are delivering to but almost always ranges between N1,000 - N2,000 only.</p>
                    </Panel>
                    <Panel header="How long before I receive my order?" >
                        <p>At checkout, you can select your preferred delivery date from our list of available dates. Orders must be placed at least 24 hours before the delivery date. See our Terms for more details on our delivery service.</p>
                    </Panel>
                    <Panel header="How do I pay for my order?" >
                        <p>Payment is done online through a secure third-party financial platform.</p>
                       
                    </Panel>
                    <Panel header="Can I pay on Delivery?" >
                        <p>We currently do not offer any payment on delivery options. All payments are done online through a secure third-party financial platform. </p>
                    </Panel>
                    <Panel header="Can I return my order?" >
                        <p>If we deliver an incorrect or damaged item (damage that has been caused by us), please notify the delivery driver at the point of delivery and we will endeavour to replace the item at no additional cost to you. Once customers have taken delivery of their order and signed confirming this, they take ownership of the items and we will be unable to accept any returns.</p>
                    </Panel>
                    <Panel header="Can I cancel my order?" >
                        <p>If you would like to edit or rethink your purchase, you can do so before payment is made. Please note that once payment is made, we begin processing and therefore your order can’t be cancelled, edited, or refunded. </p>
                    </Panel>
                    <Panel header="How do I contact you?" >
                        <p>We are always happy to hear from you. You can call us on 07037468275 or send an email to <a href="mailto:orders@farcrowdy.com" target="_blank" rel="noopener noreferrer">orders@farcrowdy.com</a> and we will respond promptly.</p>
                    </Panel>
                 
            </Collapse>
        </div>
    )
}

export default FAQ;